Description
---

We need to test out [`celery`](http://docs.celeryproject.org/en/latest/index.html)
as a job queue and scheduling system.

To make things easier we employ [`docker`](https://www.docker.com/)
containers for provisioning.

We are also using a fork situated here:
```
git clone git@github.com:rtodea/build-a-saas-app-with-flask.git
```

General Provisioning
---
Docker images:

1. [`redis`](https://hub.docker.com/_/redis/)
2. [`celery`](https://hub.docker.com/_/celery/)

Starting
---
```
docker run --link REDIS_CONTAINER_NAME:redis -e CELERY_BROKER_URL=redis://redis --name celery -d celery
```

Accessing
---
```
docker exec -it /bin/bash celery
```